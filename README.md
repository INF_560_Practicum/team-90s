# Project Description

Nowadays recommendation systems for large-scale businesses are commonly based on business popularity or reviews instead of customer’s interests. This project builds a store recommendation system for Rio Galeao Airport passengers based on their ‘potential friends’ in the database. It combines both usual business interests (popularity) and customer’s interests.

The basic idea of this recommendation system is to find similar routes of the target passenger and recommend businesses based on these similar routes. This project contains data processing, clustering, similarity analysis, and recommendation algorithms. This project’s goal is to enable stores, restaurants and other services in the airport to attract more customers via an efficient recommendation system to enhance the quality of the airport service.  

Function for dashboard:  
1. Generate heatmap for particular terminal, level within a time period.  
2. Map location points and stay points for passengers.  
3. Show similar passengers for a particular mac id.  
4. Show recommendations for a particular mac id.  
5. Show if a particulat mac id is a potential VIP customer.  

### Notice about the HeatMap Function
Because generating heatmap will use the whole dataset which is very large, so we didn't upload it on BitBucket. So this function can't be used. We just upload a smal (2019-12-27.txt) file in kiana_data folder for example.  

---

## Source Data of Preprocess Folder

All of the source data files are in JSON format.

1. business_level2.json, business_level3.json: Business source data from Rio Galeao official website.
2. business_data2.json: Business data for Rio Galeao Airport Level 2, including their name, latitude, longitude, and type. Data is extracted from file business_level2.json.
3. business_data3.json: Business data for Rio Galeao Airport Level 3, including their name, latitude, longitude, and type. Data is extracted from file business_level3.json.
4. clean_data_merge.txt: Preprocessed data, each line includes mac id, location points, and its time and level.
5. stay_points.txt: Each line contains mac id, original location tracking points, stay points.
6. stay_points_business.txt: Each line contains mac id, businesses correspond with original location tracking points, businesses correspond with stay points.
7. business_idf.txt: Inverse Document Frequency for each business.
8. business_ratings.txt: Ratings for each business.

---

## Source Code of Preprocess Folder

All the source code files are implemented in Python 3.5 environment and are in Python format.

1. extract_valid.py: Data cleaning and data preprocessing. Filter, and construct usable data.
2. reorgnize_data.py: Re-organized raw data by date, and output data in different dates.
3. staypoints.py: Construct stay points for each passenger based on the original location tracking points.
4. sptobusiness.py: Map the business with passengers' original location tracking points and stay points.
5. businessidf.py: Calculate the Inverse Document Frequency for each business.
6. business_stars.py: Calculate the ratings for each business.
7. similarity.py: Calculate the similarities between the target passenger and the passengers in the database.

---  

## Source Data of End Product

All of the source data files are in JSON format.  

1. static folder: pictures, css, js files for frontend.
2. templates folder: HTML file for frontend.
3. input_file folder: source data
	1. clean_data_2019-11-30.txt: Test dataset for end product, each line includes mac id, location points, and its time and level.  
	2. 2019-11-30_stay_points_business.txt: Test dataset for end product, each line contains mac id, businesses correspond with original location tracking points, businesses correspond with stay points.   
	3. business_data2.json: Business data for Rio Galeao Airport Level 2, including their name, latitude, longitude, and type. Data is extracted from file business_level2.json.  
	4. business_data3.json: Business data for Rio Galeao Airport Level 3, including their name, latitude, longitude, and type. Data is extracted from file business_level3.json.  
	5. business_idf.txt: Inverse Document Frequency for each business.  
	6. vip.txt: Possible VIP passengers' mac id.  
	7. point_and_business_11_30.txt: Test dataset, each line includes mac id, location points and its corresponding business, stay points and its corresponding business.  
	8. stay_points_business.txt: Train dataset for end product, each line contains mac id, businesses correspond with original location tracking points, businesses correspond with stay points.  
	9. top_30.json: File for frontend examples.  
	10. u_user_business_rate.csv: Passengers and business ratings matrix of train dataset, in pandas dataframe format.  
	11. final_user.json: Passengers and business ratings matrix of train dataset after fillNA with business ratings, in pandas dataframe format.  
	12. u_business_ratings.json: Original business ratings data, for cold start problem.  
	13. kiana_data/2019-12-27.txt: Sample dataset for heatmap function.  

---  

## Source Code of End Product  

All the source code files are implemented in Python 3.5 environment and are in Python format.  

1. requirements.txt: Environment requirements file.  
2. app.py: Dashborad start up file.  
3. find_track.py: Return tracks of original location points and stay points for one input mac id.  
4. heatmap.py: Generate heatmap for Rio Airport within time period.  
5. recommend.py: Return potential VIP status, similar passengers, recommendations for one input mac id.  
6. stay_points.py: For dashboard examples.  

### In order to properly run dashboard on local machine, several changes mentioned below must be done  
1. Flask structure must be set properly including 'static', 'templates'.  
2. In app.py, in function "queryHeatmap", the return url needs to be set to the localhost.  
3. In home.html & demo.html, every fetch API call, the parameter url needs to be set to localhost or other url properly.  
4. (Optional)To display business img, business_img folder needs to be under ../static/business_img.  
