#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json


# In[6]:


def write_file(file):
    for line in file:
        new_file_name = json.loads(line)['localtime'][:10]
        new_file_name = new_file_name + '.txt'
        with open(new_file_name, 'a') as f:
            f.write(line)


# In[7]:


for i in range(0, 39):
    if i < 10:
        file_name = '/root/kiana_data/tps2/rio_bq_201900000000000' + str(i)
    else:
        file_name = '/root/kiana_data/tps2/rio_bq_20190000000000' + str(i)
    print('started processing file: '+ file_name +'......')
    with open(file_name) as file:
        write_file(file)
    print('finished processing file: ' + file_name)


# In[11]:


for i in range(51, 181):
    if i <100:
        file_name = '/root/kiana_data/tps2/tps20000000000' + str(i)
    else:
        file_name = '/root/kiana_data/tps2/tps2000000000' + str(i)
    print('started processing file: '+ file_name +'......')
    with open(file_name, 'r') as file:
        write_file(file)
    print('finished processing file: ' + file_name)


# In[ ]:




