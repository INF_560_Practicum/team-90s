#!/usr/bin/env python
# coding: utf-8

# README
"""
use: find stay points for each user based on their original track locations
input: input file name (original track file), output file name (stay points file)
output: output json file (stay points)
input json file format for each line: {'mac id': [[lat, lng, time, level], [lat, lng, time, level], ...]}
output json file format for each line: {'mac id': [[track points], [stay points]]} (track format: [lat, lng, time, level], stay point format: [lat, lng, level])

example for using
--------------------------------------------
from staypoints import get_sp_dict
get_sp_dict("data.txt", "stay_points.txt")
"""


import json
import pandas as pd
from datetime import datetime
from math import radians, cos, sin, asin, sqrt

dis_threh = 20
time_threh = 300

 
def get_dis(lat1, lon1, lat2, lon2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
 
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371
    return c * r * 1000


def get_sp(sp):
    lat, lon = 0, 0
    for each in sp:
        lat += each[0]
        lon += each[1]
    return lat / len(sp), lon / len(sp)


def get_sp_list(track_list):
    i = 0
    stay_points = list()
    point_num = len(track_list)
    while i < point_num:
        j = i + 1
        temp_sp = [[track_list[i][0], track_list[i][1], track_list[i][3]]]
        while j < point_num:
            floor1, floor2 = track_list[i][3], track_list[j][3]
            dis = get_dis(track_list[i][0], track_list[i][1], track_list[j][0], track_list[j][1])
#             print(dis, i, j)
            if dis > dis_threh or floor1 != floor2:
                last = datetime.strptime(str(track_list[i][2]),"%Y-%m-%d %H:%M:%S")
                now = datetime.strptime(str(track_list[j][2]),"%Y-%m-%d %H:%M:%S")
                time_gap = (now - last).seconds
#                 print(time_gap, i, j)
                if time_gap > time_threh or floor1 != floor2:
                    if len(temp_sp) > 1 and temp_sp[0][2] != 'Level 4':
                        lat, lon = get_sp(temp_sp)
                        stay_points.append([lat, lon, temp_sp[0][2]])
                i = j
                break
            temp_sp.append([track_list[j][0], track_list[j][1], floor2])
            j += 1
        i = j

    if len(temp_sp) > 1 and temp_sp[0][2] != 'Level 4':
        lat, lon = get_sp(temp_sp)
        stay_points.append([lat, lon, temp_sp[0][2]])
    
#     print(len(stay_points))
    return stay_points


def get_dict(mac_id, stay_points):
    stay_dict = dict()
    stay_dict[mac_id] = stay_points
    return stay_dict


def write_data(data, file):
    with open(file, 'a') as out:
        json.dump(data, out)
        out.write('\n')
    out.close()


def add_origin_points(mac_id, track_list, stay_points):
    stay_dict = dict()
    stay_dict[mac_id] = [track_list, stay_points]
    return stay_dict


def get_sp_dict(in_file, out_file):
    with open(in_file) as f:
        # index = 0
        for line in f.readlines():
            track = json.loads(line)

            mac_id = list(track.keys())[0]
            track_list = track[mac_id]
            stay_points = get_sp_list(track_list)

            if stay_points:
    #             stay_dict = get_dict(mac_id, stay_points)
    #             write_data(stay_dict)

    #         draw_map(track_list, stay_points)
                stay_dict = add_origin_points(mac_id, track_list, stay_points)
                write_data(stay_dict, out_file)
    #         index += 1
    #         if index > 3:
    #             break

    f.close()