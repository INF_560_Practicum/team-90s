#!/usr/bin/env python
# coding: utf-8

# README
"""
use: get business IDF file
input: input file name (stay points business file), output file name (business IDF file)
output: business IDF file
input json file format for each line: {'mac id': [[track business], [stay point business]]}
output json file format: {'business name': num of visitors} (special key: 'user_num', value is total num of visitors)

example for using
--------------------------------------------
from businessidf import get_business_idf
get_business_idf('stay_points_business.txt', 'business_idf.txt')
"""


import json

def get_business_idf(in_file, out_file):
    business_data2, business_data3 = list(), list()
    with open('data/business_data2.json', 'r') as f:
        business_data2 = json.load(f)
    f.close()

    with open('data/business_data3.json', 'r') as f:
        business_data3 = json.load(f)
    f.close()

    business_dict = dict()
    for each in business_data2:
        business_dict[each[0]] = 0
    for each in business_data3:
        business_dict[each[0]] = 0

    users = 0
    with open(in_file, 'r') as f:
        for line in f.readlines():
            users += 1
            data = json.loads(line)
            businesses = list(data.values())[0][0]
            for each in businesses:
                business_dict[each] += 1
            businesses = list(data.values())[0][1]
            for each in businesses:
                business_dict[each] += 1
    f.close()

    business_dict['user_num'] = users
    with open(out_file, 'w') as f:
        json.dump(business_dict, f)
    f.close()
