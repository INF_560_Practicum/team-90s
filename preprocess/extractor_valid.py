#!/usr/bin/env python
# coding: utf-8

# # Data cleaning
# ### 1.read all piersul data

# In[1]:


pwd


# In[2]:


from pyspark import SparkContext
from pyspark.sql import SQLContext
import numpy as np
import pandas as pd
import json
import datetime
import time


# In[3]:


from operator import itemgetter
from pyspark.sql.functions import col, coalesce, lit


# In[4]:


sc = SparkContext('local[*]')


# In[5]:


sqlContext = SQLContext(sc)


# In[6]:


lines = sc.textFile('/root/kiana_data/merge_data.txt')
start = time.time()


# ### 2.construct a sparkDataFrame where all JSON obejects included
# only consider level 2,3

# In[7]:


rdd = lines.filter(lambda x: '2019-11-17' in x).map(lambda x: x.replace("\'", "\'"))
df = sqlContext.read.json(rdd)
df.show()
print(df.count())
# rdd.collect()


# ### 3.convert selected data into rdd, use mapReduce to make further selections

# In[8]:


def show_distinct(x):
    return (x.ClientMacAddr, (x.lat, x.lng, x.localtime[:19], x.Level))


# In[9]:


data = df.rdd.map(show_distinct)


# In[10]:


a=data.groupByKey().map(lambda x: (x[0], sorted(list(set(x[1])), key=itemgetter(2))))


# In[11]:


a.take(10)


# ## 4. 
# ### 'clean data' -> time diff between two locations is less than a threshold; data point in the airport range
# ### manually draw a polygon of airport region

# In[12]:


from shapely.geometry import Point, Polygon


# In[13]:


poly = Polygon([[-22.808982, -43.252395], [-22.814186, -43.244520], [-22.814383, -43.244798], [-22.818253, -43.239154], [-22.818540, -43.239475], [-22.817166, -43.241623], [-22.819314, -43.243313], [-22.818656, -43.244399], [-22.811736, -43.254612]])


# In[14]:


dateFormat = '%Y-%m-%d %H:%M:%S'
fiftenMinus = 900
eightHours = 28800
cleanNum = 0
dirtyNum = 0
time_count = 0
area_count = 0
quantity_count = 0
for x in a.collect():
    if len(x[1]) >= 3:
        # one MacID occurred more than twice exclusively
        # dump this row to 'dirty_data.txt'
        # otherwise treat as clean data
            # one mac id has at least 3 locations and no out of boundary locations 
        with open('clean_data_merge.txt', 'a') as f:
            f.write(json.dumps({x[0]:x[1]})+'\n')
        cleanNum += 1
    else:
        quantity_count += 1
        # dump current row to 'dirty_data.txt'
        with open('dirty_data_merge.txt', 'a') as f:
            f.write(json.dumps({x[0]:x[1]})+'\n')
            dirtyNum += 1


# In[15]:


print('dirty: ', str(dirtyNum))
print('clean: ', str(cleanNum))
print('quantity:', str(quantity_count))
print('duration: ', time.time()-start)


# In[16]:


print('total: ', str(len(a.collect())))

