#!/usr/bin/env python
# coding: utf-8

# README
"""
use: return a similarity dataframe between target user and all users in database
input: a user dictionary (foramt: user = {'mac id': [track business], [stay point business]}), stay point business file name, business idf file name
output: similarity dataframe

example for using
--------------------------------------------
from similarity import get_similarity_df
user = {'mac': ['KFC1', 'KFC2'], ['KFC3']}
df = get_similarity_df(user, "stay_points_business.txt", "business_idf.txt")
"""


import math
import json
import pandas as pd


def get_similar(u1, u2):
    user1, user2 = u1, u2
    if len(u1) > len(u2):
        user1, user2 = u2, u1
    
    similar = set()
    businesses1 = set(list(user1.values())[0])
    businesses2 = set(list(user2.values())[0])
    for business in businesses1:
        if business in businesses2:
            similar.add(business)
    return similar


def factor(length):
    return math.pow(2, length - 1)


def IDF(user_num, visit_num):
    return math.log(user_num / visit_num)


def get_similarity(u1, u2, similar, business_dict):
    similarity = 0
    length = len(similar)
    len_factor = factor(length)  # length-dependent factor
    
    for business in similar:
        similarity += IDF(business_dict['user_num'], business_dict[business])  # Inverse Document Frequency

    similarity *= len_factor
    similarity /= len(list(u2.values())[0])
#     similarity /= len(list(u1.values())[0]) + len(list(u2.values())[0])
    return similarity


def construct_user(data):
    businesses = list(data.values())[0][0] + list(data.values())[0][1]
    user = dict()
    mac = list(data.keys())[0]
    user[mac] = set(businesses)
    return mac, user


def normalization(max_s, min_s, x):
    if max_s - min_s <= 0:
        return 0
    return (x - min_s) / (max_s - min_s)


def get_df(mac1, sp_file):
    macs = list()
    with open(sp_file, 'r') as f:
        for line in f.readlines():
            data = json.loads(line)
            macs.append(list(data.keys())[0])
    f.close()
    df = pd.DataFrame(columns=macs, index=[mac1])
    return df


def get_business(business_file):
    business_dict = dict()
    with open(business_file, 'r') as f:
        business_dict = json.load(f)
    f.close()
    return business_dict


# input: a user dictionary (foramt: user = {'mac id': [track business], [stay point business]}), stay point business file name, business idf file name
# output: similarity dataframe
def get_similarity_df(user, sp_file, business_file):
    mac1, u1 = construct_user(user)
    df = get_df(mac1, sp_file)
    business_dict = get_business(business_file)
    min_s = float('inf')
    max_s = get_similarity(u1, u1, get_similar(u1, u1), business_dict)
    similarity_dict = dict()
    
    with open(sp_file, 'r') as f:
        for line in f.readlines():
            data = json.loads(line)
            mac2, u2 = construct_user(data)
            similarity_dict[mac2] = get_similarity(u1, u2, get_similar(u1, u2), business_dict)
            min_s = min(min_s, similarity_dict[mac2])

        for mac2 in similarity_dict:
            x = similarity_dict[mac2]
            similarity = normalization(max_s, min_s, x)
            df.at[mac1, mac2] = similarity
    f.close()
    return df
