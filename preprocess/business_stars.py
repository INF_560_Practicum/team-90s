#!/usr/bin/env python
# coding: utf-8

# In[253]:


from csv import reader,writer
import pandas as pd
import numpy as np
import statistics
import math
import json
import random
from scipy.stats import skewnorm


# In[ ]:


###
### Use information from google map as main reference
### if certain business in airport has a rating in google map, directly use it
### if not, but the same business has other stores in the city, take an average of these ratings
### for others, do imputation (use the average of its category)


# In[247]:


raw_file = list(reader(open("business.csv")))[1:]

business = []
for line in raw_file:
    
    new_line = [line[0], float(line[1]), str(line[2]), line[3]]    
    business.append(new_line)
business[:20]


# In[248]:


few_reviews = []
for line in business:
    if line[2] == '1':
        few_reviews.append(line)

few_reviews


# In[249]:


def splitData(business):
    
    business_rest = []
    business_shop = []
    business_serv = []
    
    for line in business:
        if line[3] == "restaurant":
            business_rest.append(line)
            
        elif line[3] == "shop":
            business_shop.append(line)
            
        else:
            business_serv.append(line)
            
    return business_rest, business_shop, business_serv

business_rest, business_shop, business_serv = splitData(business)


# In[250]:


def calculation(data):
    
    new_data = []
    for line in data:
        if line[1] != 0:
            new_data.append(line)
    
    columns = list(zip(*new_data))
        
    mean = sum(columns[1])/len(columns[1])
    std = statistics.stdev(columns[1])
        
    return (mean,std)

rest_s = calculation(business_rest)
shop_s = calculation(business_shop)
serv_s = calculation(business_serv)
print(rest_s)
print(shop_s)
print(serv_s)


# In[251]:


def getNewData(data):
    
    stats = calculation(data)
    #for missing business ratings, use mean if its category as expected value
    
    data_imputed = []
    for line in data:
        if line[1] == 0:
            data_imputed.append([line[0], stats[0], line[2], line[3]])
        else:
            data_imputed.append(line)
            
    #for business with no more than 3 reviewes, give some penalties and benefits for the ratings (mius/plus one std)
    data_penalized = []
    for line in data_imputed:
        if line[2] == '1':
            
            if line[1] == 1:
                new_value = 1 + stats[1]
            elif line[1] == 5:
                new_value = 5 - stats[1]
            else:
                new_value = line[1]
            data_penalized.append([line[0],new_value,line[2],line[3]])
            
        else:
            data_penalized.append(line)
    
    return data_penalized


# In[252]:


new_business = getNewData(business_shop)+getNewData(business_serv)+getNewData(business_rest)

business_stars = []
for line in new_business:
    business_stars.append(line[:2])

business_stars


# In[257]:


def convertToJson(pair):

    jsonObject = json.dumps({pair[0]: pair[1]})

    return jsonObject


# In[258]:


output = ""
for out in business_stars:
    output += convertToJson(out) + "\n"
output = output[:-1]

with open("business_ratings.txt", 'w') as out:
    out.write(output)

