#!/usr/bin/env python
# coding: utf-8

# README
"""
use: convert stay point to business
input: input file name (stay points file), output file name (user business file)
output: user business file
input json file format for each line: {'mac id': [[track points], [stay points]]} (track format: [lat, lng, time, level], stay point format: [lat, lng, level])
output json file format for each line: {'mac id': [[track business], [stay point business]]}

example for using
--------------------------------------------
from sptobusiness import translate
translate("stay_points.txt", "stay_points_business.txt")
"""


import json
from math import radians, cos, sin, asin, sqrt


def get_dis(lat1, lon1, lat2, lon2):
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
 
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371
    return c * r * 1000


def find_business(lat1, lng1, business_data):
    min_dis = 10
    business_name = ""
    for business in business_data:
        distance = get_dis(lat1, lng1, business[1], business[2])
        threshold = 10
        if business[0] == "Dufry - Duty Free":
            threshold = 15
        if distance < threshold and distance < min_dis:
            business_name = business[0]
            min_dis = distance
    return business_name


def write_data(data, file):
    with open(file, 'a') as out_f:
        json.dump(data, out_f)
        out_f.write('\n')
    out_f.close()


def translate(in_file, out_file):
    business_data2, business_data3 = list(), list()
    with open('data/business_data2.json', 'r') as f:
        business_data2 = json.load(f)
    f.close()

    with open('data/business_data3.json', 'r') as f:
        business_data3 = json.load(f)
    f.close()

    final_data=dict()
    # index = 0
    with open(in_file, 'r') as f:
        for line in f.readlines():
    #         index += 1
    #         if index > 3:
    #             break
            data = json.loads(line)
            mac = list(data.keys())[0]
            business_fps = list()
            business_sps = list()
            business_dict = dict()
            business_fp = None
            business_sp = None
            fps = data[mac][0]
            sps = data[mac][1]
            for fp in fps:
                if fp[3] == "Level 2":
                    business_fp = find_business(fp[0], fp[1], business_data2)
                if fp[3] == "Level 3":
                    business_fp = find_business(fp[0], fp[1], business_data3)
                if business_fp and not business_fp.startswith('Port') and not business_fp.startswith('Banheiro'):
                    business_fps.append(business_fp)
            for sp in sps:
                if sp[2] == "Level 2":
                    business_sp = find_business(sp[0], sp[1], business_data2)
                if sp[2] == "Level 3":
                    business_sp = find_business(sp[0], sp[1], business_data3)
                if business_sp and not business_sp.startswith('Port') and not business_sp.startswith('Banheiro'):
                    business_sps.append(business_sp)

            if business_fps or business_sps:
                business_dict[mac] = [business_fps, business_sps]
                write_data(business_dict, out_file)
    f.close()
