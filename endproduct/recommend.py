#!/usr/bin/env python
# coding: utf-8


import csv
import json
import math
import numpy as np
import pandas as pd
from datetime import datetime
from pyspark import SparkContext
from math import radians, cos, sin, asin, sqrt

origin_data = "./input_file/clean_data_2019-11-30.txt"
business_data_level2 = "./input_file/business_data2.json"
business_data_level3 = "./input_file/business_data3.json"
sp_business_file = "./input_file/stay_points_business.txt"
business_idf_file = "./input_file/business_idf.txt"
train_file_name = './input_file/u_user_business_rate.csv'
business_ratings_file = './input_file/u_business_ratings.json'
final_user_file = './input_file/final_user.json'
vip_file = './input_file/vip.txt'
top_business = ['Mania de Churrasco', "L'Occitane", 'Doog Hot Dog']

dis_threh = 20
time_threh = 300


def find(target_mac):
    result = list()
    
    with open(origin_data, 'r') as f:
        for line in f.readlines():
            track = json.loads(line)
            mac_id = list(track.keys())[0]
            track_list = track[mac_id]
            if mac_id == target_mac:
                result = track_list
                break
    f.close()
    
    if not result:
        print("mac id not found")
    return result


def get_dis(lat1, lng1, lat2, lng2):
    lng1, lat1, lng2, lat2 = map(radians, [lng1, lat1, lng2, lat2])
 
    dlng = lng2 - lng1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlng / 2)**2
    c = 2 * asin(sqrt(a))
    r = 6371
    return c * r * 1000


def get_sp(sp):
    lat, lng = 0, 0
    for each in sp:
        lat += each[0]
        lng += each[1]
    return lat / len(sp), lng / len(sp)


def get_sp_list(track_list):
    i = 0

    stay_points = list()
    point_num = len(track_list)
    while i < point_num:
        j = i + 1
        temp_sp = [[track_list[i][0], track_list[i][1], track_list[i][3]]]
        while j < point_num:
            floor1, floor2 = track_list[i][3], track_list[j][3]
            dis = get_dis(track_list[i][0], track_list[i][1], track_list[j][0], track_list[j][1])
            if dis > dis_threh or floor1 != floor2:
                last = datetime.strptime(str(track_list[i][2]),"%Y-%m-%d %H:%M:%S")
                now = datetime.strptime(str(track_list[j][2]),"%Y-%m-%d %H:%M:%S")
                time_gap = (now - last).seconds
                if time_gap > time_threh or floor1 != floor2:
                    if len(temp_sp) > 1 and temp_sp[0][2] != 'Level 4':
                        lat, lon = get_sp(temp_sp)
                        stay_points.append([lat, lon, temp_sp[0][2]])
                i = j
                break
            temp_sp.append([track_list[j][0], track_list[j][1], floor2])
            j += 1
        i = j

    if len(temp_sp) > 1 and temp_sp[0][2] != 'Level 4':
        lat, lon = get_sp(temp_sp)
        stay_points.append([lat, lon, temp_sp[0][2]])

    return stay_points


def get_dict(mac_id, stay_points):
    stay_dict = dict()
    stay_dict[mac_id] = stay_points
    return stay_dict


def add_origin_points(mac_id, track_list, stay_points):
    stay_dict = dict()
    stay_dict[mac_id] = [track_list, stay_points]
    return stay_dict


def find_business(lat, lng, business_data):
    min_dis = 10
    business_name = ""
    for business in business_data:
        distance = get_dis(lat, lng, business[1], business[2])
        threshold = 10
        if business[0] == "Dufry - Duty Free":
            threshold = 15
        if distance < threshold and distance < min_dis:
            business_name = business[0]
            min_dis = distance
    return business_name


def get_business_data():
    business_data2, business_data3 = list(), list()
    
    with open(business_data_level2, 'r') as f:
        business_data2 = json.load(f)
    f.close()
    
    with open(business_data_level3, 'r') as f:
        business_data3 = json.load(f)
    f.close()
    
    return business_data2, business_data3


def sp_to_business(mac, data):
    business_data2, business_data3 = get_business_data()
    business_fps = list()
    business_sps = list()
    business_dict = dict()
    business_fp = None
    business_sp = None
    fps = data[mac][0]
    sps = data[mac][1]
    
    for fp in fps:
        if fp[3] == "Level 2":
            business_fp = find_business(fp[0], fp[1], business_data2)
        if fp[3] == "Level 3":
            business_fp = find_business(fp[0], fp[1], business_data3)
        if business_fp:
            business_fps.append(business_fp)
    
    for sp in sps:
        if sp[2] == "Level 2":
            business_sp = find_business(sp[0], sp[1], business_data2)
        if sp[2] == "Level 3":
            business_sp = find_business(sp[0], sp[1], business_data3)
        if business_sp:
            business_sps.append(business_sp)
    
    business_dict[mac] = list()
    if business_fps or business_sps:
        business_dict[mac] = [business_fps, business_sps]
    
    return business_dict


def get_similar(u1, u2):
    user1, user2 = u1, u2
    if len(u1) > len(u2):
        user1, user2 = u2, u1
    
    similar = set()
    businesses1 = set(list(user1.values())[0])
    businesses2 = set(list(user2.values())[0])
    for business in businesses1:
        if business in businesses2:
            similar.add(business)
    return similar


def factor(length):
    return math.pow(2, length - 1)


def IDF(user_num, visit_num):
    if visit_num <= 0:
        return math.log(user_num)
    return math.log(user_num / visit_num)


def get_similarity(u1, u2, similar, business_dict):
    similarity = 0
    length = len(similar)
    len_factor = factor(length)  # length-dependent factor
    
    for business in similar:
        similarity += IDF(business_dict['user_num'], business_dict[business])  # Inverse Document Frequency

    similarity *= len_factor
    similarity /= len(list(u2.values())[0])
#     similarity /= len(list(u1.values())[0]) + len(list(u2.values())[0])
    return similarity


def construct_user(data):
    user = dict()
    mac = list(data.keys())[0]
    if not list(data.values())[0]:
        user[mac] = set()
        return mac, user
    
    businesses = list(data.values())[0][0] + list(data.values())[0][1]
    user[mac] = list(set(businesses))
    return mac, user


def normalization(max_s, min_s, x):
    if max_s - min_s <= 0:
        if max_s < 0 and x < 0:
            return - x
        return 0
    return (x - min_s) / (max_s - min_s)


def get_df(mac1):
    macs = list()
    with open(sp_business_file, 'r') as f:
        for line in f.readlines():
            data = json.loads(line)
            macs.append(list(data.keys())[0])
    f.close()
    df = pd.DataFrame(columns=macs, index=[mac1])
    return df


def get_business():
    business_dict = dict()
    with open(business_idf_file, 'r') as f:
        business_dict = json.load(f)
    f.close()
    return business_dict


def get_similarity_df(user):
    mac1, u1 = construct_user(user)
    
    df = get_df(mac1)
    business_dict = get_business()
    
    min_s = float('inf')
    max_s = get_similarity(u1, u1, get_similar(u1, u1), business_dict)
    similarity_dict = dict()
    
    with open(sp_business_file, 'r') as f:
        for line in f.readlines():
            data = json.loads(line)
            mac2, u2 = construct_user(data)
            if len(u1[mac1]) >= len(u2[mac2]):
                similarity_dict[mac2] = 0
            else:
                similarity_dict[mac2] = get_similarity(u1, u2, get_similar(u1, u2), business_dict)
            min_s = min(min_s, similarity_dict[mac2])
        
        for mac2 in similarity_dict:
            x = similarity_dict[mac2]
            similarity = normalization(max_s, min_s, x)
            df.at[mac1, mac2] = similarity
    f.close()
    return  u1[mac1], df


def find_top_n_neighbours(df, n, mac):
    """ Find top n most similar users from user similarity matrix """
    df = df.T
    df = df.sort_values(by=[mac], ascending=False)[0:n].T
    df = df.apply(lambda x: pd.Series(x.index, index=['top{}'.format(i) for i in range(1, n + 1)]), axis=1)
    return df


# Required matrix: final, top_n, business_user, final_user, similarity_with_user
def get_recommendations(user):
    try:
        """ Return top_n_recommendation business """
        # Create user similarity matrix from user business matrix
        all_b, similarity_with_user = main_func(user)
        if similarity_with_user is None:
            return False, None, top_business
        
        # Generate top_n from user similarity matrix or preload from json
        top_n = find_top_n_neighbours(similarity_with_user, 20, user)
        train_file = pd.read_csv(train_file_name)
        final = pd.pivot_table(train_file, values='rating', index='user', columns='business')
        final_user = pd.read_json(final_user_file)
        business_user = train_file.groupby(by='user')['business'].apply(lambda x: ','.join(x))
        business_seen_by_user = all_b
        
        vips = set()
        is_vip = False
        with open(vip_file, 'r') as f:
            vips = set(json.load(f))
        f.close()
        for i in range(top_n.shape[1]):
            if top_n.iloc[0][i] in vips:
                is_vip = True
                break
        
        top3 = find_top_n_neighbours(similarity_with_user, 3, user)
        top3 = top3[top3.index == user].values.squeeze().tolist()
        top3 = business_user[business_user.index.isin(top3)].to_dict()
        for each in top3:
            top3[each] = top3[each].split(',')
        # If business_seen_by_user is empty, return default top 3 businesses
        if not business_seen_by_user:
            return is_vip, top3, top_business
        
        # Retrive all businesses from neighbors but not seen by target user
        a = top_n[top_n.index == user].values
        b = a.squeeze().tolist()
        d = business_user[business_user.index.isin(b)]
        l = ','.join(d.values)
        business_seen_by_similar_users = l.split(',')
        business_under_consideration = list(set(business_seen_by_similar_users)-set(list(map(str, business_seen_by_user))))
        
        # Generate scores based on user correlations and adjust scores
        score = []
        for item in business_under_consideration:
            c = final_user.loc[:,item]
            d = c[c.index.isin(b)]
            f = d[d.notnull()]
            index = f.index.values.squeeze().tolist()
            # corration between user and some other users
            corr = similarity_with_user.loc[user,index]
            fin = pd.concat([f, corr], axis=1)
            fin.columns = ['adg_score','correlation']
            fin['score']=fin.apply(lambda x:x['adg_score'] * x['correlation'],axis=1)
            nume = fin['score'].sum()
            deno = fin['correlation'].sum()
            final_score = nume/deno
            score.append(final_score)
        
        # Sort businesses based on their scores and output highest 3 businesses
        data = pd.DataFrame({'business':business_under_consideration,'score':score})
        top_n_recommendation = data.sort_values(by='score',ascending=False).head(3)
        business_names = top_n_recommendation.business.values.tolist()
        # IF recommendation system fails, recommend the user with default top 3 businesses
        if len(business_names) < 3:
            business_names = business_names + top_business[:3 - len(business_names)]

        return is_vip, top3, business_names
    except:
        return False, None, top_business


def main_func(mac_id):
    tracks = find(mac_id)
    stay_points = get_sp_list(tracks)
    stay_points_dict = add_origin_points(mac_id, tracks, stay_points)
    business_dict = sp_to_business(mac_id, stay_points_dict)
    if not business_dict[mac_id]:
        return None, None
    business, similarity_df = get_similarity_df(business_dict)
    return business, similarity_df
