import json

def find_track(target_mac):
    result = dict()
    with open('./input_file/point_and_business_11_30.txt', 'r') as f:
        for line in f.readlines():
            data = json.loads(line)
            mac = list(data.keys())[0]
            if mac == target_mac:
                result = data
    f.close()
    if not result:
        print('mac id not found')
        return None
    return result
