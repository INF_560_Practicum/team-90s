import json
import folium
from folium.plugins import HeatMap
import time
import os


def draw_map(level, terminal, d1, d2):
    file = d1.split(" ")[0]
    a = time.strptime(d1, '%Y-%m-%d %H:%M:%S')
    b = time.strptime(d2, '%Y-%m-%d %H:%M:%S')
    with open('../kiana_data/' + file + '.txt', 'r') as f:
        json_data = []
        for line in f.readlines():
            json_data.append(json.loads(line))

    location_data = {}
    visited = set()
    for i in json_data:
        if i['Building'] == terminal and i['Level'] == level:
            t = time.strptime(i['localtime'][0:19], '%Y-%m-%d %H:%M:%S')
            if a <= t <= b:
                if (round(i['lat'], 5), round(i['lng'], 5)) not in location_data:
                    location_data[(round(i['lat'], 5), round(i['lng'], 5))] = 0
                if  i['ClientMacAddr'] not in visited:
                    location_data[(round(i['lat'], 5), round(i['lng'], 5))] = location_data[(round(i['lat'], 5), round(i['lng'], 5))] + 1
    
    data = []
    for i in location_data:
        data.append([i[0],i[1],float(location_data[i])])
    map_osm = folium.Map(location=[-22.8,-43.2],zoom_start=10)    #绘制Map，开始缩放程度是5倍
    HeatMap(data,radius=5, gradient={.25: 'purple',.4: 'blue', .5: 'lime', .575: 'yellow', .65: 'red'}).add_to(map_osm)  # 将热力图添加到前面建立的map里
    pwd = os.getcwd()
    path = pwd + "/templates/heatmap.html"
    map_osm.save(path) 




