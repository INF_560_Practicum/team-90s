from flask import Flask, render_template, request, jsonify
from flask_cors import CORS, cross_origin
from stay_points import get_points
from recommend import get_recommendations
from find_track import find_track
from heatmap import draw_map
import json
import re
import datetime
app = Flask(__name__)
app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy   dog'
app.config['CORS_HEADERS'] = 'Content-Type'

cors = CORS(app, resources={r"/foo": {"origins": "http://localhost:port"}})


@app.route('/')
@app.route('/homepage')
def homepage():
    return render_template('home.html')


@app.route('/')
@app.route('/heatmap')
def heatmap():
    return render_template('heatmap.html')


@app.route('/query', methods=['POST', 'GET'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def query():
    data = get_points()
    print(data)
    return jsonify(data)


@app.route('/queryRecommendation', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def queryRecommendation():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        macid = json_data.get('macID')
        if valid_mac(macid):
            recommendations = get_recommendations(macid)
            if recommendations:
                return jsonify({'valid': {'recommendation': recommendations[2], 'vip': recommendations[0], 'similar': recommendations[1]}})
        return jsonify({'invalid': "macID entered is either invalid or not in the database."})


@app.route('/queryPoints', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def queryPoints():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        macid = json_data.get('macID')
        if valid_mac(macid):
            info = find_track(macid)
            if info:
                return jsonify({'valid': info})
        return jsonify({'invalid': "macID entered is either invalid or not in the database."})


@app.route('/querySimilar', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def querySimilar():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        macid = json_data.get('macID')
        if valid_mac(macid):
            info = find_similar(macid)
            if info:
                print(info)
                return jsonify({'valid': info})
        return jsonify({'invalid': "macID entered is either invalid or not in the database."})


@app.route('/queryHeatmap', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def queryHeatmap():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        terminal = json_data.get('Terminal')
        level = json_data.get('Level')
        date = json_data.get('Date')
        time = json_data.get('Time')
        range = int(json_data.get('Range'))
        d1 = date + ' ' + time + ':00'
        tmp = datetime.datetime.strptime(d1, '%Y-%m-%d %H:%M:%S') + datetime.timedelta(minutes=range)
        print(tmp)
        draw_map(level, terminal, d1, tmp.strftime("%Y-%m-%d %H:%M:%S"))
    return "http://35.223.157.34:5000/heatmap"


@app.route('/demo')
def demo():
    return render_template('demo.html')


def valid_mac(mac):
    regex = r"^\s*([0-9a-fA-F]{2,2}:){5,5}[0-9a-fA-F]{2,2}\s*$"
    if re.match(regex, mac):
        return True
    return False


def find_similar(mac):
    with open('./input_file/top_30.json') as file:
        file = json.load(file)
        top1 = file.get('top1')
        top2 = file.get('top2')
        top3 = file.get('top3')
        return [top1.get(mac), top2.get(mac), top3.get(mac)]


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)


